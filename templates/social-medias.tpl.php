<!-- social media -->
<div class="row socials">
    <div class="col-md-12">
        <ul id="social-medias-<?php echo $data['nid']; ?>" class="social-media social-<?php print variable_get('button_style','default'); ?>" data-node="<?php echo $data['nid']; ?>">
            <?php foreach( $data['social_medias_enabled'] as $social_media_enabled) : ?>
                <?php switch($social_media_enabled) {
                    case 'facebook' :
                ?>
                        <li class="social sm-facebook" data-count="<?php echo intval($data['social_medias_has_counter'][$social_media_enabled]); ?>" data-url="<?php echo $data['absolute_url']; ?>" data-type="facebook" data-title="<?php echo $data['title']; ?>" data-lang="<?php echo $data['language'];?>" data-show-counter="<?php echo $data['show_counter']; ?>"><?php if($data['show_counter']) : ?><span class="counter"><b>0</b></span><?php endif;?><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <?php break;
                    case 'twitter' :
                ?>
                        <li class="social sm-twitter" data-count="<?php echo intval($data['social_medias_has_counter'][$social_media_enabled]); ?>" data-url="<?php if($data['short_url']) echo $data['short_url']; else echo $data['absolute_url']; ?>" data-type="twitter" data-title="<?php echo $data['title']; ?>" data-show-counter="<?php echo $data['show_counter']; ?>" data-lang="<?php echo $data['language'];?>" data-via="<?php echo $data['via'];?>" data-hashtags="<?php echo $data['hashtags']; ?>"><?php if($data['show_counter']) : ?><span class="counter"><b>0</b></span><?php endif;?><a href="#" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <?php break;
                    case 'google' :
                ?>
                        <li class="social sm-google" data-count="<?php echo intval($data['social_medias_has_counter'][$social_media_enabled]); ?>" data-url="<?php echo $data['absolute_url']; ?>" data-type="google" data-title="<?php echo $data['title']; ?>" data-lang="<?php echo $data['language'];?>" data-show-counter="<?php echo $data['show_counter']; ?>"><?php if($data['show_counter']) : ?><span class="counter"><b>0</b></span><?php endif;?><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                <?php break;
                    case 'linkedin' :
                ?>
                        <li class="social sm-linkedin" data-count="<?php echo intval($data['social_medias_has_counter'][$social_media_enabled]); ?>" data-url="<?php echo $data['absolute_url']; ?>" data-type="linkedin" data-title="<?php echo $data['title']; ?>" data-lang="<?php echo $data['language'];?>" data-show-counter="<?php echo $data['show_counter']; ?>"><?php if($data['show_counter']) : ?><span class="counter"><b>0</b></span><?php endif;?><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                <?php break;
                    case 'stumbleupon' :
                ?>
                        <li class="social sm-stumbleupon" data-count="<?php echo intval($data['social_medias_has_counter'][$social_media_enabled]); ?>" data-url="<?php echo $data['absolute_url']; ?>" data-type="stumbleupon" data-title="<?php echo $data['title']; ?>" data-lang="<?php echo $data['language'];?>" data-show-counter="<?php echo $data['show_counter']; ?>"><?php if($data['show_counter']) : ?><span class="counter"><b>0</b></span><?php endif;?><a href="#" class="icoStumbleupon" title="Stumbleupon"><i class="fa fa-stumbleupon"></i></a></li>
                <?php break;
                    case 'pinterest' :
                ?>
                        <li class="social sm-pinterest" data-count="<?php echo intval($data['social_medias_has_counter'][$social_media_enabled]); ?>" data-media="<?php echo $data['media']; ?>" data-url="<?php echo $data['absolute_url']; ?>" data-type="pinterest" data-title="<?php echo $data['title']; ?>" data-show-counter="<?php echo $data['show_counter']; ?>" data-lang="<?php echo $data['language'];?>"><?php if($data['show_counter']) : ?><span class="counter"><b>0</b></span><?php endif;?><a href="#" class="icoPinterest" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                <?php break;
                    case 'delicious' :
                        ?>
                        <li class="social sm-delicious" data-count="<?php echo intval($data['social_medias_has_counter'][$social_media_enabled]); ?>" data-url="<?php echo $data['absolute_url']; ?>" data-type="delicious" data-title="<?php echo $data['title']; ?>" data-lang="<?php echo $data['language'];?>" data-show-counter="<?php echo $data['show_counter']; ?>"><?php if($data['show_counter']) : ?><span class="counter"><b>0</b></span><?php endif;?><a href="#" class="icoDelicious" title="Delicious"><i class="fa fa-delicious"></i></a></li>
                        <?php break;
                    case 'reddit' :
                        ?>
                        <li class="social sm-reddit" data-count="<?php echo intval($data['social_medias_has_counter'][$social_media_enabled]); ?>" data-url="<?php echo $data['absolute_url']; ?>" data-type="reddit" data-title="<?php echo $data['title']; ?>" data-lang="<?php echo $data['language'];?>" data-show-counter="<?php echo $data['show_counter']; ?>"><?php if($data['show_counter']) : ?><span class="counter"><b>0</b></span><?php endif;?><a href="#" class="icoReddit" title="Reddit"><i class="fa fa-reddit"></i></a></li>
                        <?php break;
                    case 'vk' :
                        ?>
                        <li class="social sm-vk" data-count="<?php echo intval($data['social_medias_has_counter'][$social_media_enabled]); ?>" data-media="<?php echo $data['media']; ?>" data-url="<?php echo $data['absolute_url']; ?>" data-type="vk" data-title="<?php echo $data['title']; ?>" data-lang="<?php echo $data['language'];?>" data-show-counter="<?php echo $data['show_counter']; ?>"><?php if($data['show_counter']) : ?><span class="counter"><b>0</b></span><?php endif;?><a href="#" class="icoVk" title="VK"><i class="fa fa-vk"></i></a></li>
                        <?php break;
                    case 'buffer' :
                        ?>
                        <li class="social sm-buffer" data-count="<?php echo intval($data['social_medias_has_counter'][$social_media_enabled]); ?>" data-url="<?php echo $data['absolute_url']; ?>" data-type="buffer" data-title="<?php echo $data['title']; ?>" data-lang="<?php echo $data['language'];?>" data-show-counter="<?php echo $data['show_counter']; ?>"><?php if($data['show_counter']) : ?><span class="counter"><b>0</b></span><?php endif;?><a href="#" class="icoBuffer" title="Buffer"><i class="soc buffer"></i></a></li>
                        <?php break;
                    case 'xing' :
                        ?>
                        <li class="social sm-xing" <?php if(!is_null($data['xing_follow'])) : ?> data-xing-follow="<?php echo $data['xing_follow']; ?>"<?php endif; ?> data-count="<?php echo intval($data['social_medias_has_counter'][$social_media_enabled]); ?>" data-url="<?php echo $data['absolute_url']; ?>" data-show-counter="<?php echo $data['show_counter']; ?>" data-type="xing" data-title="<?php echo $data['title']; ?>" data-lang="<?php echo $data['language'];?>"><?php if($data['show_counter']) : ?><span class="counter"><b>0</b></span><?php endif;?><a href="#" class="icoXing" title="Xing"><i class="fa fa-xing"></i></a></li>
                        <?php break;
                } ?>
            <?php endforeach; ?>
            <li class="total"><span class="s-counter"><b class="counter"><?php echo $data['total_count'];?></b></span><span class="text">Total share</span></li>
        </ul>
    </div>
</div>
<!-- -->