<?php

class SocialMediasCounter {

    protected $serviceLink = array(
        "facebook"    => "http://graph.facebook.com/?id=", //"https://api.facebook.com/method/links.getStats?format=json&urls=",
        "twitter"     => "http://urls.api.twitter.com/1/urls/count.json?url=",
        "xing"        => "https://www.xing-share.com/app/share?op=get_share_button;counter=top;url=", //"https://www.xing-share.com/spi/shares/statistics",
        "google"      => "https://apis.google.com/u/0/se/0/_/+1/sharebutton?plusShare=true&url=",
        "delicious"   => "http://feeds.del.icio.us/v2/json/urlinfo/data?url=", //"http://feeds.delicious.com/v2/json/urlinfo/data?url=",
        "reddit"      => "https://www.reddit.com/api/info.json?&url=",
        "buffer"      => "https://api.bufferapp.com/1/links/shares.json?url=",
        "vk"          => "https://vk.com/share.php?act=count&index=1&url=",
        "stumbleupon" => "http://www.stumbleupon.com/services/1.01/badge.getinfo?url=",
        "linkedin"    => "https://www.linkedin.com/countserv/count/share?format=json&url=",
        "pinterest"   => "http://api.pinterest.com/v1/urls/count.json?url=",
    );


     private function get_service_data($url) {

        if(function_exists('curl_version')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_USERAGENT, 'Social_medias/1.0 by cwplus');
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 1);
            $data = curl_exec($ch);
            curl_close($ch);
        } else {
            $data = @file_get_contents($url);
        }
        return $data;
    }

    public function get_service($service,$url,$short=false){
        if(isset($this->serviceLink[$service])){
           // if($service == 'facebook') return $this->get_facebook($url);
            $url = $this->serviceLink[$service].$url;
            $data = $this->get_service_data($url);
            switch($service) {
                case 'facebook'   :
                    $json = json_decode($data, true);
                    $count = isset($json['share'])?intval($json['share']['comment_count']) + intval($json['share']['share_count']):0;
                    break;
                case 'twitter'    :  //is disable
                    $json = json_decode($data, true);
                    $count = isset($json['count'])?intval($json['count']):0;
                    break;
                case 'xing'       :
                    //$json = json_decode($data, true);
                    preg_match("'<span class=\"xing-count top\">(.*?)</span>'si", $data, $match);
                    $count = isset($match[1])?intval($match[1]):0;
                    break;
                case 'google'     :
                    //$json = json_decode($data, true);
                    preg_match( '/ld:\[[^,]+,\[\d+,(\d+),/', $data, $matches);
                    $count = isset($matches[1])?intval($matches[1]):0;
                    break;
                case 'delicious'  :   // problem
                    $json = json_decode($data, true);
                    $count = isset($json[0])?(int) $json[0]->total_posts:0;
                    break;
                case 'reddit'     :
                    $data = @file_get_contents($url);
                    $json = json_decode($data, true);
                    $ups = 0;
                    $downs = 0;
                    foreach($json['data']['children'] as $child) {
                        $ups+= (int) $child['data']['ups'];
                        $downs+= (int) $child['data']['downs'];
                    }
                    $count = $ups - $downs;
                    break;
                case 'buffer'     :
                    $json = json_decode($data, true);
                    $count = $json['shares'];
                    break;
                case 'vk'         :
                    preg_match('/^VK.Share.count\(\d+,\s+(\d+)\);$/i', $data, $matches);
                    $count = $matches[1];
                    break;
                case 'stumbleupon':
                    $json = json_decode($data, true);
                    $count = isset($json['result']['views'])?intval($json['result']['views']):0;
                    break;
                case 'linkedin'   :
                    $json = json_decode($data, true);
                    $count = isset($json['count'])?intval($json['count']):0;
                    break;
                case 'pinterest'  :
                    $data = substr( $data, 13, -1);
                    $json = json_decode($data, true);
                    $count = isset($json['count'])?intval($json['count']):0;
                    break;
            }
            return ($short)?$this->shorterTotal($count):$count;
        }
        return 0;
    }

    private function shorterTotal($num) {
        if ($num >= 1e12) {
            $num = "<b class='counter'>".number_format(($num / 1e12), 0, ".", ""). "</b> T";
        } else if ($num >= 1e12) {
            $num = "<b class='counter'>".number_format(($num / 1e9), 0, ".", ""). "</b> G";
        } else if ($num >= 1e6) {
            $num = "<b class='counter'>".number_format(($num / 1e6), 0, ".", "")."</b> M";
        } else if ($num >= 1e3) {
            $num = "<b class='counter'>".number_format(($num / 1e3), 0, ".", "")."</b> k";
        } else {
            $num = "<b class='counter'>".$num."</b>";
        }
        return $num;
    }
}


/*function get_facebook($url,$short=false) {
 	$json_string = file_get_contents('https://api.facebook.com/method/links.getStats?format=json&urls=' . $url );
 	$json = json_decode($json_string, true);
 	if(isset($json[0]['total_count'])){
	 		return ($short)?shorterTotal(intval( $json[0]['total_count'] )):intval( $json[0]['total_count'] );
 	} else { return 0;}
}

function get_twitter($url,$short=false) {
    $json_string = file_get_contents('http://urls.api.twitter.com/1/urls/count.json?url=' . $url);
    $json = json_decode($json_string, true);
 	if(isset($json['count'])){
        return ($short)?shorterTotal(intval( $json['count'] )):intval( $json['count'] );
 	} else {return 0;}
}

function get_xing($url,$short=false){
// CURL initiieren & Parameter Setzen
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, 'https://www.xing-share.com/spi/shares/statistics');
    curl_setopt($ch,CURLOPT_POST, 1);
    curl_setopt($ch,CURLOPT_POSTFIELDS, 'url='.$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

// CURL Resultate in String schreiben und schließen
    $curl_results = curl_exec($ch);
    curl_close($ch);

// JSON Parsen
    $json = json_decode($curl_results,true);

// Rückgabe vorbereiten
    if($json['share_counter'])
        return ($short)?shorterTotal(intval( $json['share_counter'] )):intval( $json['share_counter'] );
    return 0;
}

function get_google($url,$short=false)
{
    $json_string = file_get_contents('https://apis.google.com/u/0/se/0/_/+1/sharebutton?plusShare=true&url='.$url);
    preg_match( '/ld:\[[^,]+,\[\d+,(\d+),/', $json_string, $matches);
    if(isset($matches[1])){
        $count = (int) $matches[1];
        return ($short)?shorterTotal($count):$count;
    }
    return 0;
}

function get_delicious($url,$short=false)
{
    $json_string = file_get_contents('http://feeds.delicious.com/v2/json/urlinfo/data?url='.$url);
    $json = json_decode($json_string,true);
    if(isset($json[0])){
        $count = (int) $json[0]->total_posts;
        return ($short)?shorterTotal($count):$count;
    }
    return 0;
}

function get_reddit($url,$short=false)
{
    $json_string = file_get_contents('http://www.reddit.com/api/info.json?&url='.$url);
    $data = json_decode($json_string,true);
    $ups = 0;
    $downs = 0;
    foreach($data->data->children as $child) {
        $ups+= (int) $child->data->ups;
        $downs+= (int) $child->data->downs;
    }
    $count = $ups - $downs;
    return ($short)?shorterTotal($count):$count;
}

function get_buffer($url,$short=false)
{
    $json_string = file_get_contents('https://api.bufferapp.com/1/links/shares.json?url='.$url);
    $data = json_decode($json_string,true);
    $count = $data->shares;
    return ($short)?shorterTotal($count):$count;
}

function get_vk($url,$short=false)
{
    $json_string = file_get_contents('https://vk.com/share.php?act=count&index=1&url='.$url);
    preg_match('/^VK.Share.count\(\d+,\s+(\d+)\);$/i', $json_string, $matches);
    $count = $matches[1];
    return ($short)?shorterTotal($count):$count;
}

function get_stumbleupon($url,$short=false) {
	$json_string = file_get_contents('http://www.stumbleupon.com/services/1.01/badge.getinfo?url='.$url);
	$json = json_decode($json_string, true);
	if (isset($json['result']['views'])) {
        $ret = intval($json['result']['views']);
		return ($short)?shorterTotal($ret):$ret;
	} else {return 0;}
}

function get_linkedin($url,$short=false) {
	$json_string = file_get_contents('https://www.linkedin.com/countserv/count/share?format=json&url=' . $url);
	$json = json_decode($json_string, true);
	if(isset($json['count'])){
		return ($short)?shorterTotal(intval( $json['count'] )):intval( $json['count'] );
	} else {return 0;}
}

function get_pinterest($url,$short = false) {
    $json_string = file_get_contents('http://api.pinterest.com/v1/urls/count.json?url=' . $url);
    $json_string = substr( $json_string, 13, -1);
    $json = json_decode($json_string, true);
    if(isset($json['count'])){
        return ($short)?shorterTotal(intval( $json['count'] )):intval( $json['count'] );
    } else {return 0;}
}
*/




/*function formatNumberAbbreviation($number) {
    if ($number != 0 && preg_match('/B|M|K/i', $number)) {
        var_dump($number);
        switch (strtolower(substr($number, -1))) {
            case 'k':
                $number*=1000;
                break;
            case 'm':
                $number*=1000000;
                break;
            case 'b':
                $number*=1000000000;
                break;
        }
    }
    return $number;
}*/