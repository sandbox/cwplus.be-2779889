/**
 * Created by khalid on 26/07/16.
 */

(function ($) {
    var sm = Drupal.settings.sm;
    var totalShare = 0;
    $.fn.smPopup = function (options) {
        // Default params
        var defaults =
        {
            "url": null,
            "type": 'facebook',
            "title": null,
            "via": "ElhakymKhalid",
            "media": null,
            "lang": 'en-US',
            "count": null,
            "hashtags": null,
            "xing_follow": null,
            "show_counter":null,
            "counters": Object.keys(sm.social_medias_has_counter).length,
            "ajax_counter_url": sm.ajax_counter_url,
        };
        var parametres = $.extend(defaults, options);

        function shorterTotal(num) {
            if (num >= 1e12) {
                num = "<b class='counter'>" + (num / 1e12).toFixed(0) + "</b> T"
            } else if (num >= 1e9) {
                num = "<b class='counter'>" + (num / 1e9).toFixed(0) + "</b> G"
            } else if (num >= 1e6) {
                num = "<b class='counter'>" + (num / 1e6).toFixed(0) + "</b> M"
            } else if (num >= 1e3) {
                num = "<b class='counter'>" + (num / 1e3).toFixed(0) + "</b> k"
            } else {
                num = "<b>"+num+"</b>";
            }
            return num;
        }

        //console.log($(".social-media"));
        this.each(function (i) {
            var span_total_counter = $(this).find('.total .s-counter');
            var node = $(this).data('node');
            var elemnt_id = $(this).attr('id');
            var $this = $('#'+elemnt_id+' .social');
            var _i = 0;
            var totalShare = 0;
            $this.each(function (j) {
                var $$this = $(this);

                // If option on the link attributs
                var url = ($(this).attr('data-url') != undefined && $(this).attr('data-url') != '') ? $(this).attr('data-url') : parametres.url;
                var type = ($(this).attr('data-type') != undefined && $(this).attr('data-type') != '') ? $(this).attr('data-type') : parametres.type;
                var title = ($(this).attr('data-title') != undefined && $(this).attr('data-title') != '') ? $(this).attr('data-title') : parametres.title;
                var via = ($(this).attr('data-via') != undefined && $(this).attr('data-via') != '') ? $(this).attr('data-via') : parametres.via;
                var count = ($(this).attr('data-count') != undefined && $(this).attr('data-count') != '') ? $(this).attr('data-count') : parametres.count;
                count = parseInt(count);
                var ajax_counter_url = ($(this).attr('data-ajax-counter-url') != undefined && $(this).attr('data-ajax-counter-url') != '') ? $(this).attr('data-ajax-counter-url') : parametres.ajax_counter_url;
                var media = ($(this).attr('data-media') != undefined && $(this).attr('data-media') != '') ? $(this).attr('data-media') : parametres.media;
                var lang = ($(this).attr('data-lang') != undefined && $(this).attr('data-lang') != '') ? $(this).attr('data-lang') : parametres.lang;
                var hashtags = ($(this).attr('data-hashtags') != undefined && $(this).attr('data-hashtags') != '') ? $(this).attr('data-hashtags') : parametres.hashtags;
                var xing_follow = ($(this).attr('data-xing-follow') != undefined && $(this).attr('data-xing-follow') != '') ? $(this).attr('data-xing-follow') : parametres.xing_follow;
                var show_counter = ($(this).attr('data-show-counter') != undefined && $(this).attr('data-show-counter') != '') ? $(this).attr('data-show-counter') : parametres.show_counter;
                var popupWidth = 0;
                var popupHeight = 0;
                var shareUrl = '';
                // Url use to share
                url = (url != null) ? url : document.URL;
                //$URL = url;

                switch (type) {
                    case 'twitter': // Twitter share
                        shareUrl = 'https://twitter.com/intent/tweet?original_referer=' + encodeURIComponent(document.URL) + '&url=' + encodeURIComponent(url);
                        if(title != null) shareUrl += '&text=' + encodeURIComponent(title);
                        if(hashtags != null) shareUrl += '&hashtags='+encodeURIComponent(hashtags);
                        if(via != null) shareUrl += '&via='+via;
                        break;
                    case 'google': // Google + share
                        shareUrl = 'https://plus.google.com/share?url=' + url;
                        if(lang != null) shareUrl += '&hl=' + lang;
                        break;
                    case 'xing': // Xing
                        shareUrl = 'https://www.xing.com/spi/shares/new?url=' + encodeURIComponent(url);
                        if(xing_follow != null) shareUrl += '&follow_url='+ encodeURIComponent(xing_follow);
                        shareUrl = (lang != null) ? shareUrl + '&lang=' + lang : shareUrl;
                        break;
                    case 'linkedin' : // Linkedin share
                        shareUrl = 'https://www.linkedin.com/shareArticle?url=' + url;
                        break;
                    case 'stumbleupon': // StumbleUpon share
                        //shareUrl = 'http://www.stumbleupon.com/submiturl=' + url;
                        shareUrl = 'http://www.stumbleupon.com/submit=' + url;
                        if(title != null) shareUrl += '&title=' + title;
                        break;
                    case 'pinterest': // Pinterest share
                        shareUrl = 'http://www.pinterest.com/pin/create/button/?url=' + url;
                        if(media != null) shareUrl += '&media=' + media;
                        if(title != null) shareUrl += '&description=' + title;
                        break;
                    case 'delicious': // delicious share
                        shareUrl = 'http://www.delicious.com/save?v=5&noui&jump=close&url=' + encodeURIComponent(url);
                        if(title != null) shareUrl += '&title=' + encodeURIComponent(title);
                        break;
                    case 'reddit': // reddit share
                        shareUrl = 'https://reddit.com/submit?url=' + encodeURIComponent(url);
                        if(title != null) shareUrl += '&title=' + encodeURIComponent(title);
                        break;
                    case 'vk': // vk share
                        shareUrl = 'http://vk.com/share.php?url=' + encodeURIComponent(url);
                        if(media != null) shareUrl += '&image=' + media;
                        if(title != null) shareUrl += '&title=' + encodeURIComponent(title);
                        break;
                    case 'buffer': // buffer share
                        shareUrl = 'https://buffer.com/add?url=' + encodeURIComponent(url);
                        if(title != null) shareUrl += '&text=' + encodeURIComponent(title);
                        break;
                    default: // Default Facebook share
                        shareUrl = 'https://www.facebook.com/sharer/sharer.php?u=' + url;
                        break;
                }
                if (count) {
                    _i++;
                    /**
                     * for test, uncomment this line.
                     * url = 'http://www.google.com';
                     */
                    url = 'http://www.google.com';
                    $.getJSON(ajax_counter_url + '?url=' + url + '&service='+type+'&node='+node).success(function (resultdata) {
                        var counter = (isNaN(parseInt(resultdata.count))) ? 0 : parseInt(resultdata.count);
                        totalShare += counter;
                        if(show_counter) $$this.find('.counter').html(shorterTotal(counter));
                        if(_i == parametres.counters) {
                            span_total_counter.html(shorterTotal(totalShare));
                        }
                    });

                }

                // Click to Action Open Popup Share
                $(this).on('click tap', function (e) {
                    e = (e ? e : window.event);
                    var t = (e.target ? e.target : e.srcElement),
                        width = t.data-width || 800,
                        height = t.data-height || 500;

                    // popup position
                    var
                        px = Math.floor(((screen.availWidth || 1024) - width) / 2),
                        py = Math.floor(((screen.availHeight || 700) - height) / 2);

                    // open popup
                    var popup = window.open(shareUrl, "SocialPopup",
                        "width="+width+",height="+height+
                        ",left="+px+",top="+py+
                        ",resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes");

                    if (popup) {
                        popup.focus();
                        if (e.preventDefault) e.preventDefault();
                        e.returnValue = false;
                    }

                    return !!popup;
                });
            });

        });
    };
})(jQuery);

jQuery(".social-media").smPopup();