This module gives you an opportunity to add social icons to your content and make it shareable.

Our module provides you with 11 social networks :
Twitter - Facebook - Google - Linkedin - Stumbleupon - Pinterest - Xing - Delicious - Reddit - Vk - Buffer.

==========================================
Social name 	Counter 	Share button
==========================================
Twitter 	    No 	        Yes
------------------------------------------
Facebook 	    Yes 	    Yes
------------------------------------------
Google 	        Yes 	    Yes
------------------------------------------
Linkedin 	    Yes 	    Yes
------------------------------------------
Stumbleupon 	Yes 	    Yes
------------------------------------------
Pinterest 	    Yes 	    Yes
------------------------------------------
Xing 	        Yes 	    Yes
------------------------------------------
Delicious 	    No 	        Yes
------------------------------------------
Reddit 	        Yes 	    Yes
------------------------------------------
Vk 	            Yes 	    Yes
------------------------------------------
Buffer 	        Yes 	    Yes
------------------------------------------




Configuration :
goto : admin/config/cwplus/social-medias

this module is tested with drupal 7.